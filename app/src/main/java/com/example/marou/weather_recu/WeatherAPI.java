package com.example.marou.weather_recu;

import android.net.Uri;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class WeatherAPI {
    private final String BASE_URL = "https://api.openweathermap.org/data/2.5/weather";
    private final String APPID = "644da4f2a1231c6611d2e2d8abb1fc90";
    private final String IMAGE_BASE_URL = "http://openweathermap.org/img/w/";

    ArrayList<Weather> getWeathers(ArrayList<String> cities) {
        return doCall(cities);
    }

    /*https://api.openweathermap.org/data/2.5/weather?q=Barcelona&units=metric&lang=ca&appid=f83265590654458d800741b9b861c518*/

    private String getUrlPage(String city) {
        Uri builtUri = Uri.parse(BASE_URL)
                .buildUpon()
                .appendQueryParameter("q", city)
                .appendQueryParameter("units", "metric")
                .appendQueryParameter("lang", "ca")
                .appendQueryParameter("appid", APPID)
                .build();
        return builtUri.toString();
    }

    private String getUrlImage(String code) {
        Uri builtUri = Uri.parse(IMAGE_BASE_URL)
                .buildUpon()
                .appendPath("/" + code + ".png")
                .build();
        return builtUri.toString();
    }

    private ArrayList<Weather> doCall(ArrayList<String> cities) {
        ArrayList<Weather> data = new ArrayList<>();

        for (int i = 0; i < cities.size(); i++) {
            try {
                String url = getUrlPage(cities.get(i));
                String JsonResponse = HttpUtils.get(url);
                data.add(processJson(JsonResponse));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return data;
    }

    private Weather processJson(String jsonResponse) {
        Weather weather = new Weather();
        try {
            JSONObject jsonWeather = new JSONObject(jsonResponse);


            weather.setId(jsonWeather.getInt("id"));
            weather.setImageUrl(getUrlImage(jsonWeather.getJSONArray("weather")
                    .getJSONObject(0).getString("icon")));
            weather.setLon(jsonWeather.getJSONObject("coord").getDouble("lon"));
            weather.setLat(jsonWeather.getJSONObject("coord").getDouble("lat"));
            weather.setWeather(getUrlImage(jsonWeather.getJSONArray("weather")
                    .getJSONObject(0).getString("description")));
            weather.setWeather(jsonWeather.getString("name"));
            weather.setPais(jsonWeather.getJSONObject("sys").getString("country"));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return weather;
    }

}
